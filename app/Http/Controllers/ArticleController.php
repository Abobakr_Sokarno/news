<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Articles;
use App\Comments;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

	public function index()
    {
    	$articles=Articles::latest()->where('publish', 'yes')->get();

    	return view('news', compact('articles'));

    }

	public function create ()
	{
		return view('createarticles');
	}

	 public function store(Request $request)
    {
    	$this->validate(request(),[
            'title'=>'required|min:5',
            'body'=>'required|min:15',
            'url'=>'required|image|mimes:jpg,jpeg,png,gif|max:2048'
        ]);
        
        $img_name=time() . '.' . $request->url->getClientOriginalExtension();

        $articles= new Articles;
        $articles->user_id=auth()->id();
        $articles->title=request('title');
        $articles->body=request('body');
        $articles->url=$img_name;
        $articles->save();

        $request->url->move(public_path('uploads'), $img_name);

        session()->flash('message', 'Done Sending Your Article Please Wait Until it is approved by Our Admins ');

        return redirect('/allnews');
    }

    public function show ($id)
    {
    	$article = Articles::find($id);
    	return view('article', compact('article'));
    }
}
