<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Articles;
use App\comments;
use DB;

class AdminsController extends Controller
{
    public function showPanel()
    {
    	return view('dashboard.admin_panel');
    }
    
    public function showUsers()	
    {
    	$users=User::get();
        return view('dashboard.showUsers', compact('users'));
    }

    public function addRole(Request $request)
    {
    	 $user=User::where('email', $request['email'])->first();
        $user->roles()->detach();

        if($request['role_user'])
        {
            $user->roles()->attach(Role::where('name', 'user')->first());
        }
        if($request['role_admin'])
        {
            $user->roles()->attach(Role::where('name', 'admin')->first());
        }

        return redirect()->back();
    }

    public function addUser()
    {
    	return view('dashboard.addUser');
    }

    public function register (Request $request)
    {
    	$this->validate(request(),[
            'name'=>'required|min:5|max:25',
            'email'=>'required|email',
            'password'=>'required|confirmed'
        ]);

    	$pass=request(['password']);
        $user= new User;
        $user->name=request('name');
        $user->email=request('email');
        $user->password=bcrypt($pass['password']);
        $user->save();

        //to asign user role for a new user
        $user->roles()->attach(Role::where('name', 'user')->first());

        session()->flash('message', 'User created Successfully...');

    	return redirect('/users');
    }

    public function showArticles()
    {
    	$articles=Articles::latest()->get();

    	return view('dashboard.allArticles', compact('articles'));
    }

    public function showEditPage($id)
    {
    	$article=Articles::find($id);
    	return view('dashboard.editArticle', compact('article'));
    }

    public function update(Request $request, $id)
    {   
        $this->validate(request(),[
            'title'=>'required',
            'body'=>'required'
        ]);

        $task=new Articles;
        $task->where('id', $id)->update(['title'=>request('title'), 'body'=>request('body')]);
        session()->flash('message', 'Post is Updated');
        return redirect('/articlesAdmin');
    }

    public function destroyArticle($id)
    {
        DB::table('articles')->where('id', $id)->delete();
        session()->flash('message', 'Post is Deleted');
        return redirect('/articlesAdmin');
    }

    public function showPendingArticles()
    {
    	$articles=Articles::latest()->where('publish', 'no')->get();

    	return view('dashboard.pendingArticles', compact('articles'));

    }

    public function changeArticleStatus($id)
    {
    	$task=new Articles;
        $task->where('id', $id)->update(['publish'=>'yes']);
        session()->flash('message', 'Article is Published');
        return redirect('/pending-articles');
    }

    public function showPendingComments()
    {
        $comments=Comments::latest()->where('publish', 'no')->get();
        return view('dashboard.pendingComments', compact('comments'));

    }

    public function changeCommentStatus($id)
    {
        $task=new Comments;
        $task->where('id', $id)->update(['publish'=>'yes']);
        session()->flash('message', 'Comment is Published');
        return redirect('/pending-comments');
    }

    public function destroyComment($id)
    {
        DB::table('comments')->where('id', $id)->delete();
        session()->flash('message', 'Comment is Deleted');
        return redirect('/pending-comments');
    }
}
