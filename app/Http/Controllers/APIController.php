<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Articles;
use App\Comments;
use App\User ; 


class APIController extends Controller
{
    public function get_all_news(Request $request){
        $articles=Articles::latest()->where('publish', 'yes')->get();
        return $articles ; 
    }
    
    public function add_news(Request $request)
    {
        if($request->hasFile('url') && isset($request['title']) && isset($request['body']) && isset($request['user_id']))
        {
            $check_for_user = User::where('id',$request['user_id'])->first();
            if(! $check_for_user)
            {
                return response()->json(['message'=>'This user not found']);
            }
            
            $img_name=time() . '.' . $request->url->getClientOriginalExtension();
            $request->url->move(public_path('uploads'), $img_name);
        
            $articles= new Articles() ;
            $articles->user_id= $request['user_id']  ; 
            $articles->title=$request['title'];
            $articles->body=$request['body'];
            $articles->url=$img_name;
            $articles->save();
        }
        else{
            return response()->json(['message'=>'Failed to add new article']);
        }

        return response()->json(['message'=>'Done Sending Your Article Please Wait Until it is approved by Our Admins ']);
    }
    
    
    public function edit_news(Request $request)
    {
        
        $id = $request['id'];
        $article = Articles::where('id',$id)->first(); 
        $title = $request['title'] ; 
        $body = $request['body'] ;
        $url = $request['url'] ; 
    
        if($article)
        {
            if($request->hasFile('url'))
            {
                $img_name=time() . '.' . $request->url->getClientOriginalExtension();
                $request->url->move(public_path('uploads'), $img_name);
                $article->url=$img_name;
            }

            
            if(isset($title))
            {
                $article->title = $title ; 
            }
            if(isset($body))
            {
                $article->body = $body ; 
            }
            $article->save();
            
            return response()->json(['message'=>'article update successfully']);
        }
        else{
            return response()->json(['message'=>'article not found']);
        }
    }
    
    public function comments_on_article(Request $request, $article_id)
    {
        $comments = Comments::where('articles_id',$article_id)
                ->join('articles','comments.articles_id','=','articles.id')
                ->get();
        
        return $comments ; 
    }
    
    
    public function edit_comment(Request $request, $comment_id)
    {
        // update comment logic is to update the comment or published (yes-no)
        
        $comment = Comments::where('id',$comment_id)->first() ; 
        if(! $comment )
        {
            return response()->json(['message'=>'failed to find that comment']);
        }
        
        if(isset($request['body']))
            $comment->body = $request['body'] ; 
        
        if(isset($request['publish']))
            $comment->publish = $request['publish'] ; 
            
        $comment->save();
        return response()->json(['message'=>'comment updated successfully']) ; 
        
    }
    
    public function add_comment_to_article(Request $request)
    {
        $article_id = $request['article_id']  ; 
        
        $check_for_article = Articles::where('id',$article_id)->first();
        if($check_for_article)
        {
            if(isset($request['body']) && isset($request['user_id']))
            {
                $comment = new Comments();  
                $comment->body = $request['body'] ;
                $comment->articles_id = $article_id ; 
                $comment->user_id = $request['user_id'] ; 
                if(isset($request['publish']))
                    $comment->publish = $request['publish'];
                $comment->save(); 
                return response()->json(['message'=>'comment added successfully']) ;    
            }
            else{
                return response()->json(['message'=>'error while adding comment, check inputs']) ; 
            }
        } 
        return response()->json(['message'=>'article not found']) ; 
    }
    
    
}


/*

Add news -- done 
Edit News -- done 
Get All News -- done 
Get All comments sub of news. -- done 
Edit Comments.
Add Comment for specific news. -- done

*/
