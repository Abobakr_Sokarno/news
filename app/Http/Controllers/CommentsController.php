<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Articles;
use App\Comments;


class CommentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Articles $articles)
    {
         $this->validate(request(),[
            'body'=>'required|min:5'
        ]);

        Comments::create([
            'body'=>request('body'),
            'articles_id'=>$articles->id,
            'user_id'=>auth()->user()->id
        ]);

        session()->flash('message', 'Comment is Waiting to be approved ' . \Auth::user()->name);
        return back();
    }
}
