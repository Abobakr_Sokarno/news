<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Comments;
use App\User;

class Articles extends Model
{
    protected $fillable = ['title', 'body', 'url', 'user_id'];

	public function comments()
    {
        return $this->hasMany(Comments::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    

}
