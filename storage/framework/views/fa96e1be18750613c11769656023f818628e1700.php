<?php $__env->startSection('content'); ?>
<head>
	<title><?php echo e($article->title); ?></title>
</head>
<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(/images/img_1.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Enjoy Reading</span>
							<h1>Our Articles</h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
</header>

		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="author">
					<div class="desc animate-box">
<?php if($flash = session('message')): ?>
	<div class="alert alert-warning" role="alert">
		<b><?php echo e($flash); ?></b>
	</div>	
<?php endif; ?>
						<hr>
						<span>Published by:- <?php echo e($article->user->name); ?></span>
						<hr>
						<span>Date:-</span>
						<mark><?php echo e($article->created_at->toFormattedDateString()); ?></mark> 
						<hr>
						<span>Title:- </span><h3>"<?php echo e($article->title); ?>"</h3>
						<hr>
						<p>Body:- <?php echo e($article->body); ?>.</p>
						<hr>
					</div>
					<div class="author-inner animate-box">
						<img src="/uploads/<?php echo e($article->url); ?>" style="max-width:1200px;max-height:350px;width:600px;height:450px">
					</div>
				</div>
			</div>
		</div>
<hr>
		<label style="margin: auto 30px">Comments:- &nbsp;</label>
<?php if(count($article->comments)): ?>
		<div class="container">
			<?php $__currentLoopData = $article->comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<?php if($comment->publish === 'yes'): ?>
			<div class="row">
			<div class="col-sm-6">	
			<div class="alert alert-success">
				<div style="height: 15px">
				<span style="font-size: 13px">By:- </span><b style="font-size: 13px"><?php echo e($comment->user->name); ?></b>
				<mark style="float: right;font-size:13px "> <?php echo e($comment->created_at->diffForHumans()); ?></mark>
				</div>
				<hr>
				<div style="height:60px ">
				<center><p>"<?php echo e($comment->body); ?>"</p></center>
				 <?php if(Auth::check()): ?>
				 <?php if(Auth::user()->hasRole('admin')): ?>
				 	<a href="/comment/delete/<?php echo e($comment->id); ?>"><button class="btn btn-danger" style="height:35px;width:145px;font-size:12px; margin:-5px auto;float: right;padding: 5px 5px">Delete Comment</button></a>
				 <?php endif; ?>
				 <?php endif; ?>
				</div>
			</div>
			</div>
			</div>
			<hr>
			<?php endif; ?>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</div>
<?php else: ?>
	<div class="alert alert-primary" style="margin: auto 20px">
				No comments for that article
			</div>
<?php endif; ?>
		<hr>
		<div class="row" style="margin: auto 50px;">
		<form method="POST" action="/article/<?php echo e($article->id); ?>/comment"> 
		<?php echo e(csrf_field()); ?>

		<div class="form-group">
			<label for="body"><b>Leave your comment</b></label>
			<textarea name="body" id="body" class="form-control" rows="7" cols="10" style="width: 500px"></textarea>
		</div>
			<button type="submit" class="btn btn-success">Add Comment</button>
		</div>
		<hr>
<?php echo $__env->make('errors.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</form>	
	</div>	
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>