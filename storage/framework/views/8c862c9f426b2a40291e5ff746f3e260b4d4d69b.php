 <div class="container-fluid">
      <div class="row">
        <nav class="col-sm-3 col-md-2 hidden-xs-down bg-faded sidebar">
          <ul class="nav nav-pills flex-column">
            <li class="nav-item">
              <a class="nav-link active" href="#">Overview</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Add User</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Pending Posts</a>
            </li>
             <li class="nav-item">
              <a class="nav-link" href="#">Pending Comments</a>
            </li>
             <li class="nav-item">
              <a class="nav-link" href="#">aproved Posts</a>
            </li>
          </ul>
        </nav>
