<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Panel</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/dashboard.css" rel="stylesheet">
  </head>

  <body style="margin: -50px auto;">
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">
        <span data-feather="aperture"></span>&nbsp;Reda's NewsMagazine</a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="/logout"><button class="btn btn-info"><span data-feather="log-out" style="margin: -5px auto"></span>&nbsp;Sign out</button></a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link active" href="/admin">
                  <span data-feather="home"></span>
                  Dashboard
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/users">
                  <span data-feather="users"></span>
                  Users
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/addUser">
                  <span data-feather="user-plus"></span>
                  Add User
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/pending-articles">
                  <span data-feather="file-plus"></span>
                  Pending Articles
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/pending-comments">
                  <span data-feather="bar-chart-2"></span>
                  Pending Comments 
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/articlesAdmin">
                  <span data-feather="layers"></span>
                  All Articles
                </a>
              </li>
               <li class="nav-item">
                <a class="nav-link" href="/">
                  <button class="btn btn-warning" style="width: 200px">
                  View Site
                  </button>
                </a>
              </li>
            </ul>

          </div>
        </nav>
      </div>
    </div>
