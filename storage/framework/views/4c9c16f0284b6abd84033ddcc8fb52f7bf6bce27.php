<?php $__env->startSection('admin'); ?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
										<h2>Users</h2>
										<hr>
<?php if($flash = session('message')): ?>
	<div class="alert alert-warning" role="alert">
		<b><?php echo e($flash); ?></b>
	</div>	
<?php endif; ?>
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<table class="table table-hover">
	<tr>
		<th>id</th>
		<th>Name</th>
		<th>Email</th>
		<th>User</th>
		<th>Admin</th>
	</tr>
	<?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<form method="POST" action="/add-roles">
		<?php echo e(csrf_field()); ?>

	<tr>
		<td><?php echo e($user->id); ?></td>
		<td><?php echo e($user->name); ?></td>
		<td><?php echo e($user->email); ?></td>
		<td>
			<input type="hidden" name="email" value="<?php echo e($user->email); ?>">
			<input type="checkbox" name="role_user" onchange="this.form.submit()" <?php echo e($user->hasRole('user')?'checked': ' '); ?>>
		</td>
		<td>
			<input type="checkbox" name="role_admin" onchange="this.form.submit()" <?php echo e($user->hasRole('admin')?'checked': ' '); ?>>
		</td>
	</tr>
	</form>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table>
</div>
</main>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('dashboard.masterAdmin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>