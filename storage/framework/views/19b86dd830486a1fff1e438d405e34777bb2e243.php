<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by FreeHTML5.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="FreeHTML5.co" />
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="/css/icomoon.css">
	<!-- Themify Icons-->
	<link rel="stylesheet" href="/css/themify-icons.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="/css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="/css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="/css/owl.carousel.min.css">
	<link rel="stylesheet" href="/css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="/css/style.css">

	<!-- Modernizr JS -->
	<script src="/js/modernizr-2.6.2.min.js"></script>
	</head>
	<body>
		
	<div class="gtco-loader"></div>
	
	<div id="page">

	
	<div class="page-inner">
	<nav class="gtco-nav" role="navigation">
		<div class="gtco-container">
			
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div id="gtco-logo"><a href="/">NewsMagazine <em>.</em></a></div>
				</div>
				<div class="col-xs-8 text-right menu-1">
					<ul>
						<li><a href="/">Home</a></li>
						<li><a href="/allnews">All News</a></li>
						<li><a href="/create">Publish Articles</a></li>
						<li><a href="#">About Us</a></li>
						<li><a href="/contact-us">Contact</a></li>
						 <?php if(Auth::check()): ?>
        					<li class="btn-cta"><a href="/logout"><span>Logout! 
        						<?php echo e(Auth::user()->name); ?></span></a></li>
          				 <?php else: ?>
						<li class="btn-cta"><a href="/join-Us"><span>Join Us</span></a></li>
						 <?php endif; ?>
						 <br>
						 <?php if(Auth::check()): ?>
						 <?php if(Auth::user()->hasRole('admin')): ?>
						 <li><a href="/admin"><button class="btn btn-danger">Admin Panel</button></a></li>
						 <?php endif; ?>
						 <?php endif; ?>
					</ul>
				</div>
			</div>
			
		</div>
	</nav>