<?php $__env->startSection('content'); ?>
<head>
	<title>All News</title>
</head>
<body>
		<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(/images/img_1.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					

					<div class="row row-mt-15em">

						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Enjoy Reading</span>
							<h1>Our Articles</h1>	
						</div>
						
					</div>
							
					
				</div>
			</div>
		</div>
	</header>

<div class="gtco-section border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
<?php if($flash = session('message')): ?>
	<div class="alert alert-warning" role="alert">
		<b><?php echo e($flash); ?></b>
	</div>	
<?php endif; ?>
					<h2>All News Articles</h2>
					<p>This is an over view on Our Site's Approved Posts to be Published.</p>
				</div>
			</div>
			<div class="row">
				<?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<?php if($article->url): ?>
					<a href="/uploads/<?php echo e($article->url); ?>" class="fh5co-project-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="/uploads/<?php echo e($article->url); ?>" alt="Image" class="img-responsive">
						</figure>
					</a>
					<?php endif; ?>
						<div class="fh5co-text">
							<a href="/article/<?php echo e($article->id); ?>"><h2><?php echo e($article->title); ?></h2></a>
							<p><?php echo e($article->body); ?></p>
						</div>
					</a>
				</div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>