<?php $__env->startSection('admin'); ?>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
										<h2>Pending Articles</h2>
										<hr>
<?php if($flash = session('message')): ?>
	<div class="alert alert-warning" role="alert">
		<b><?php echo e($flash); ?></b>
	</div>	
<?php endif; ?>

	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<table class="table table-hover">
			<tr>
				<th>Id</th>
				<th>title</th>
				<th>Author</th>
				<th>Created At</th>
				<th>Publish</th>
				<th>Delete</th>
				<th>Display</th>
			</tr>
				<?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<tr>
				<td><?php echo e($article->id); ?></td>
				<td><?php echo e($article->title); ?></td>
				<td><?php echo e($article->user->name); ?></td>
				<td><?php echo e($article->created_at->toFormattedDateString()); ?></td>
				<td><a href="/changeArticleStatus/<?php echo e($article->id); ?>"><button class="btn btn-info">Publsih</button></a></td>
				<td><a href="/articles/delete/<?php echo e($article->id); ?>"><button class="btn btn-danger">Delete</button></a></td>
				<td><a href="/article/<?php echo e($article->id); ?>"><button class="btn btn-warning">Display</button></a></td>
			</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>			
		</table>
	

	</div>
</main>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('dashboard.masterAdmin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>