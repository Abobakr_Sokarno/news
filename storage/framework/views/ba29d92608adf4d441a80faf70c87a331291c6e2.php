<?php $__env->startSection('content'); ?>
<head>
	<title>Create Your Article</title>
</head>
<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(/images/img_1.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					

					<div class="row row-mt-15em">

						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Welcome To</span>
							<h1>Reda News Magazine</h1>	
						</div>

					</div>
							
					
				</div>
			</div>
		</div>
</header>	

<div class="container">
	<form method="POST" action="/articles" enctype="multipart/form-data"> 
		<?php echo e(csrf_field()); ?>

		<div class="form-group" style="width:900px">
			<label for="title"><b>Title</b></label>
			<input type="text" name="title" class="form-control" style="width: 500px">
		</div>
		<div class="form-group">
			<label for="body"><b>Body</b></label>
			<textarea name="body" id="body" class="form-control" rows="7" cols="10" style="width: 500px"></textarea>
		</div>
		<div class="form-group">
			<label for="upload"><b>Upload Image</b></label>
			<input type="file" name="url" class="form-control">
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-success">Add Post</button>
		</div>
<?php echo $__env->make('errors.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</form>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>