@extends('master')
@section('content')
<head>
	<title>{{ $article->title }}</title>
</head>
<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(/images/img_1.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Enjoy Reading</span>
							<h1>Our Articles</h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
</header>

		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="author">
					<div class="desc animate-box">
@if($flash = session('message'))
	<div class="alert alert-warning" role="alert">
		<b>{{ $flash }}</b>
	</div>	
@endif
						<hr>
						<span>Published by:- {{ $article->user->name }}</span>
						<hr>
						<span>Date:-</span>
						<mark>{{ $article->created_at->toFormattedDateString() }}</mark> 
						<hr>
						<span>Title:- </span><h3>"{{ $article->title }}"</h3>
						<hr>
						<p>Body:- {{ $article->body }}.</p>
						<hr>
					</div>
					<div class="author-inner animate-box">
						<img src="/uploads/{{ $article->url }}" style="max-width:1200px;max-height:350px;width:600px;height:450px">
					</div>
				</div>
			</div>
		</div>
<hr>
		<label style="margin: auto 30px">Comments:- &nbsp;</label>
@if(count($article->comments))
		<div class="container">
			@foreach($article->comments as $comment)
			@if($comment->publish === 'yes')
			<div class="row">
			<div class="col-sm-6">	
			<div class="alert alert-success">
				<div style="height: 15px">
				<span style="font-size: 13px">By:- </span><b style="font-size: 13px">{{ $comment->user->name }}</b>
				<mark style="float: right;font-size:13px "> {{ $comment->created_at->diffForHumans() }}</mark>
				</div>
				<hr>
				<div style="height:60px ">
				<center><p>"{{ $comment->body }}"</p></center>
				 @if(Auth::check())
				 @if(Auth::user()->hasRole('admin'))
				 	<a href="/comment/delete/{{ $comment->id }}"><button class="btn btn-danger" style="height:35px;width:145px;font-size:12px; margin:-5px auto;float: right;padding: 5px 5px">Delete Comment</button></a>
				 @endif
				 @endif
				</div>
			</div>
			</div>
			</div>
			<hr>
			@endif
			@endforeach
		</div>
@else
	<div class="alert alert-primary" style="margin: auto 20px">
				No comments for that article
			</div>
@endif
		<hr>
		<div class="row" style="margin: auto 50px;">
		<form method="POST" action="/article/{{ $article->id }}/comment"> 
		{{ csrf_field() }}
		<div class="form-group">
			<label for="body"><b>Leave your comment</b></label>
			<textarea name="body" id="body" class="form-control" rows="7" cols="10" style="width: 500px"></textarea>
		</div>
			<button type="submit" class="btn btn-success">Add Comment</button>
		</div>
		<hr>
@include('errors.errors')
	</form>	
	</div>	
	</div>
</div>
@endsection