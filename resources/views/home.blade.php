@extends('master')
@section('content')
<head>
	<title>Home</title>
</head>
<body>
	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(/images/img_1.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					

					<div class="row row-mt-15em">

						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Welcome To</span>
							<h1>Reda News Magazine</h1>	
						</div>

					</div>
							
					
				</div>
			</div>
		</div>
	</header>	

	<div class="gtco-section border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 text-left gtco-heading">
@if($flash = session('message'))
	<div class="alert alert-warning" role="alert">
		<b>{{ $flash }}</b>
	</div>	
@endif
					<h2>Articles We Include</h2>
					<p>We have all sorts of Articles, news and events covered in our news magazine, Get a cool tour in Our Site and feel free.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5">
					<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<span class="icon">
							<i class="icon-check"></i>
						</span>
						<div class="feature-copy">
							<h3>Make a Cup of Tea</h3>
							<p>ENjoy your favourite Drink reading Our Articles.</p>
						</div>
					</div>

					<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<span class="icon">
							<i class="icon-check"></i>
						</span>
						<div class="feature-copy">
							<h3>Fully Secured</h3>
							<p>You can Join Us for free and add your article.</p>
						</div>
					</div>

					<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<span class="icon">
							<i class="icon-check"></i>
						</span>
						<div class="feature-copy">
							<h3>Testing Creativity</h3>
							<p>If Your article meets our requirment it will be published online for all world.</p>
						</div>
					</div>

					<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
						<span class="icon">
							<i class="icon-check"></i>
						</span>
						<div class="feature-copy">
							<h3>Easy to Use</h3>
							<p>Our service is not complicated at all its very easy for people to use.</p>
						</div>
					</div>

				</div>
				<div class="col-md-7 macbook-wrap animate-box" data-animate-effect="fadeInRight">
					<img src="/images/macbook.png" alt="reda reda">
				</div>
			</div>
		</div>
	</div>


@endsection