@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
										<h2>Pending Comments</h2>
										<hr>
@if($flash = session('message'))
	<div class="alert alert-warning" role="alert">
		<b>{{ $flash }}</b>
	</div>	
@endif

	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<table class="table table-hover">
			<tr>
				<th>Id</th>
				<th>Comment's Body</th>
{{-- 				<th>Comment On</th>
 --}}				<th>Owner</th>
				<th>Created At</th>
				<th>Publish</th>
				<th>Delete</th>
			</tr>
				@foreach($comments as $comment)
			<tr>
				<td>{{ $comment->id }}</td>
				<td>{{ $comment->body }}</td>
{{-- 				<td>{{ $comment->article->title }}</td>
 --}}				<td>{{ $comment->user->name }}</td>
				<td>{{ $comment->created_at->toFormattedDateString() }}</td>
				<td><a href="/changeCommentStatus/{{ $comment->id }}"><button class="btn btn-info">Publsih</button></a></td>
				<td><a href="/comment/delete/{{ $comment->id }}"><button class="btn btn-danger">Delete</button></a></td>
			</tr>
				@endforeach			
		</table>
	

	</div>
</main>
@endsection
