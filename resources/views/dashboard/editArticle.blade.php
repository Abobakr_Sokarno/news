@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<h2>Edit article</h2>
	<hr>

	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		<div class="container">
  <form method="POST" action="/update/{{ $article->id }}">
  	{{ csrf_field() }}
  	<input type="hidden" name="_method" value="PATCH">
    <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">Article Title</label>
      <div class="col-sm-10">
        <input type="text" name='title' class="form-control"  value="{{ $article->title }}">
      </div>
    </div>
    <div class="form-group row">
      <label for="email" class="col-sm-2 col-form-label">Article Body</label>
      <div class="col-sm-10">
      	<textarea name="body" rows="7" cols="70">{{ $article->body }}</textarea>
      </div>
    </div>
     <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-success">Edit Article</button>
      </div>
    </div>
    @include("errors.errors")
  </form>
</div>


	</div>
</main>
@endsection