@extends('dashboard.masterAdmin')
@section('admin')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
	<h2>Dashboard</h2>
	<hr>
	<p>
		This Area is specified only for admins...
	</p>
	<hr>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
    <canvas class="my-4" id="myChart" width="400" height="180"></canvas>
</div>
</main>

@endsection