@extends('master')
@section('content')
<head>
	<title>All News</title>
</head>
<body>
		<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url(/images/img_1.jpg)">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					

					<div class="row row-mt-15em">

						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Enjoy Reading</span>
							<h1>Our Articles</h1>	
						</div>
						
					</div>
							
					
				</div>
			</div>
		</div>
	</header>

<div class="gtco-section border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
@if($flash = session('message'))
	<div class="alert alert-warning" role="alert">
		<b>{{ $flash }}</b>
	</div>	
@endif
					<h2>All News Articles</h2>
					<p>This is an over view on Our Site's Approved Posts to be Published.</p>
				</div>
			</div>
			<div class="row">
				@foreach($articles as $article)
				<div class="col-lg-4 col-md-4 col-sm-6">
					@if($article->url)
					<a href="/uploads/{{ $article->url }}" class="fh5co-project-item image-popup">
						<figure>
							<div class="overlay"><i class="ti-plus"></i></div>
							<img src="/uploads/{{ $article->url }}" alt="Image" class="img-responsive">
						</figure>
					</a>
					@endif
						<div class="fh5co-text">
							<a href="/article/{{ $article->id }}"><h2>{{ $article->title }}</h2></a>
							<p>{{ $article->body }}</p>
						</div>
					</a>
				</div>
				@endforeach
			</div>
		</div>
	</div>
@endsection