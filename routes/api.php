<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('all_news','APIController@get_all_news');
Route::post('add_news','APIController@add_news');
Route::post('edit_news','APIController@edit_news');
Route::get('get_all_comments_on_article/{article_id}','APIController@comments_on_article');
Route::post('add_comment_to_article','APIController@add_comment_to_article');
Route::post('edit_comment/{comment_id}','APIController@edit_comment');
